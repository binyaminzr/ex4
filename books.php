<?php
include "db.php"; 
include "query.php";

$db = new DB('localhost', 'example', 'root', '');
$dbc = $db->connect();
$query = new Query($dbc); 
$q = "SELECT b.title_field as book, u.name as name FROM users u, books b where b.author=u.id";
$result = $query->query($q); 
if($result->num_rows > 0){
    echo '<table>'; 
    echo '<tr><th>book name</th><th>Author name</th></tr>';
    while($row = $result->fetch_assoc()){
      echo '<tr>'; 
      echo '<td>'.$row['book'].'</td><td>'.$row['name'].'</td>';
      echo '</tr>';
    }
    echo '</table>';
  } else {
    echo "Sorry no results"; 
  }
?>